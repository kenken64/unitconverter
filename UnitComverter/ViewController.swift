//
//  ViewController.swift
//  UnitComverter
//
//  Created by Qin Guan on 30/3/17.
//  Copyright © 2017 Qin Guan. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    @IBOutlet weak var inputLabel: UILabel!
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var measurement: UIPickerView!
    @IBOutlet weak var convertedValue: UILabel!
    @IBOutlet weak var convertBtn: UIButton!
    
    var measurementConversion = ["M to KM", "KM to M"]
    var conversionMethod = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        valueTextField.delegate = self
        measurement.delegate =  self
        measurement.dataSource = self
    }
    
    @IBAction func convert(_ sender: Any) {
        print("converting")
        var inputValue: Double = Double(valueTextField.text!)!
        var answer: Double = 0
        print(conversionMethod)

        if(conversionMethod == 0){
            answer  = inputValue / 1000
        }else if(conversionMethod == 1){
            answer = inputValue * 1000
        }
        convertedValue.text = (String)(answer)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return measurementConversion.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return measurementConversion[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        conversionMethod = row
    }


}

